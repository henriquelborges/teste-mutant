module.exports.allWebsites = async (application, req, res) => {
    responseStatus = 200;
    application.app.services.service.allWebsites().then((response) => {
        res.json(response);
    }).catch((error) => {
        console.error("Error while fulling /allWebsites request. Error =", error);
        responseStatus = 500;
        res.status(responseStatus).json("Server error.");
    }).finally(() => application.config.elasticSearch.elasticSearch.add("/allWebsites", req.query, req.connection.remoteAddress, responseStatus));
}

module.exports.allUsers = async (application, req, res) => {
    //This variable selects the JSON property that will order the response array.
    let sortOption = "name";
    responseStatus = 200;
    if (typeof req.query.sort !== "undefined") {
        switch (req.query.sort) {
            case "email":
                sortOption = "email";
                break;
            case "company":
                sortOption = "company";
                break;
        }
    }
    application.app.services.service.allUsers(sortOption).then((response) => {
        res.json(response);
    }).catch((error) => {
        console.error("Error while fulling /allUsers request. Error =", error);
        responseStatus = 500;
        res.status(responseStatus).json("Server error.");
    }).finally(() => application.config.elasticSearch.elasticSearch.add("/allUsers", req.query, req.connection.remoteAddress, responseStatus));;
}

module.exports.search = async (application, req, res) => {
    //This variable selects the value of the JSON "address" property that will filter the response array.
    let name = "";
    responseStatus = 200;
    if (typeof req.query.name !== "undefined") {
        name = req.query.name;
    }

    application.app.services.service.search(name).then((response) => {
        res.json(response);
    }).catch((error) => {
        console.error("Error while fulling /search request. Error =", error);
        responseStatus = 500;
        res.status(responseStatus).json("Server error.");
    }).finally(() => application.config.elasticSearch.elasticSearch.add("/search", req.query, req.connection.remoteAddress, responseStatus));;
}