let http = require("https");
let service = {};

service.callEndpoint = async (host, path) => {
    //Creating a requisition.
    options = {
        "method": "GET",
        "hostname": host,
        "path": path
    };
    let promise = new Promise((resolve, reject) => {
        let requisition = http.request(options, (response) => {
            let chunks = [];

            response.on("data", (chunk) => {
                chunks.push(chunk);
            });

            response.on("end", () => {
                let body = Buffer.concat(chunks);
                let response = JSON.parse(body.toString());
                if (typeof response.error !== "undefined") reject(response)
                else resolve(response);
            });

            response.on("error", (error) => {
                reject(error);
            });

            response.on("timeout", (error) => {
                reject(error);
            });
        });

        requisition.on("error", (error) => {
            reject(error);
        });

        requisition.end();
    });
    return promise
}

service.allWebsites = async () => {
    return new Promise(async (resolve, reject) => {
        await service.callEndpoint("jsonplaceholder.typicode.com", "/users").then((results) => {
            let response = [];
            results.forEach((result) => response.push(result.website));
            resolve(response);
        }).catch((error) => {
            reject(error);
        });
    });
}

service.allUsers = async (sortOption) => {
    return new Promise(async (resolve, reject) => {
        await service.callEndpoint("jsonplaceholder.typicode.com", "/users").then((results) => {
            let response = [];
            results.forEach((result) => {
                item = {}
                item.name = result.name
                item.email = result.email
                item.company = result.company.name
                response.push(item)
            });

            //Sorts the response
            response.sort((a, b) => {
                if (a[sortOption] > b[sortOption]) {
                    return 1;
                }
                if (a[sortOption] < b[sortOption]) {
                    return -1;
                }
                return 0;
            });
            resolve(response);
        }).catch((error) => {
            reject(error);
        });
    });
}

service.search = async (name) => {
    return new Promise(async (resolve, reject) => {
        await service.callEndpoint("jsonplaceholder.typicode.com", "/users").then((results) => {
            resolve(results.filter((result) => result.address.suite.toLowerCase().includes(name.toLowerCase())));
        }).catch((error) => {
            reject(error);
        });
    });
}
module.exports = service;