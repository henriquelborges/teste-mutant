let server = require("./config/server.js");

//Start the server.
try {
    server.app.listen(server.port, server.host, () => {
    }).on("error", (error) => {
        console.log("Server error =", error);
    }).on("listening", () => {
        console.log("Server is online and listening on", server.host + ":" + server.port);
    }).on("request", (request) => {
        console.log("Server received a new request.");
    });

} catch (error) {
    console.log("Error on Server, error =", error);
}

//Process events
process.on("uncaughtException", (error) => {
    console.log("Application closing due an uncaughtException...");
    console.log("Error =", error);
});