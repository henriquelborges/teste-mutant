module.exports = (application) => {
	application.get("/allWebsites", (req, res) => {
		application.app.controllers.controller.allWebsites(application, req, res);
	});

	application.get("/allUsers", (req, res) => {
		application.app.controllers.controller.allUsers(application, req, res);
    });
    
    application.get("/search", (req, res) => {
		application.app.controllers.controller.search(application, req, res);
	});
}