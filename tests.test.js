let services = require("./app/services/service");
let ElasticSearch = require("./config/elasticSearch/elasticSearch");

//Tests the function resposible for collect all websites.
test("allWebsites function test", async () => {
    let expectedResult = [
        "hildegard.org",
        "anastasia.net",
        "ramiro.info",
        "kale.biz",
        "demarco.info",
        "ola.org",
        "elvis.io",
        "jacynthe.com",
        "conrad.com",
        "ambrose.net"
    ]
    expect(await services.allWebsites()).toEqual(expectedResult);
});

//Tests the function resposible for collect all users info.
test("allUsers function test", async () => {
    let expectedResult = [
        {
            "name": "Chelsey Dietrich",
            "email": "Lucio_Hettinger@annie.ca",
            "company": "Keebler LLC"
        },
        {
            "name": "Clementina DuBuque",
            "email": "Rey.Padberg@karina.biz",
            "company": "Hoeger LLC"
        },
        {
            "name": "Clementine Bauch",
            "email": "Nathan@yesenia.net",
            "company": "Romaguera-Jacobson"
        },
        {
            "name": "Ervin Howell",
            "email": "Shanna@melissa.tv",
            "company": "Deckow-Crist"
        },
        {
            "name": "Glenna Reichert",
            "email": "Chaim_McDermott@dana.io",
            "company": "Yost and Sons"
        },
        {
            "name": "Kurtis Weissnat",
            "email": "Telly.Hoeger@billy.biz",
            "company": "Johns Group"
        },
        {
            "name": "Leanne Graham",
            "email": "Sincere@april.biz",
            "company": "Romaguera-Crona"
        },
        {
            "name": "Mrs. Dennis Schulist",
            "email": "Karley_Dach@jasper.info",
            "company": "Considine-Lockman"
        },
        {
            "name": "Nicholas Runolfsdottir V",
            "email": "Sherwood@rosamond.me",
            "company": "Abernathy Group"
        },
        {
            "name": "Patricia Lebsack",
            "email": "Julianne.OConner@kory.org",
            "company": "Robel-Corkery"
        }
    ];
    expect(await services.allUsers("name")).toEqual(expectedResult);
});

//Tests the function resposible for collect all users info based on a word in the suit property of user's address info.
test("search function test", async () => {
    let expectedResult = [
        {
            "id": 2,
            "name": "Ervin Howell",
            "username": "Antonette",
            "email": "Shanna@melissa.tv",
            "address": {
                "street": "Victor Plains",
                "suite": "Suite 879",
                "city": "Wisokyburgh",
                "zipcode": "90566-7771",
                "geo": {
                    "lat": "-43.9509",
                    "lng": "-34.4618"
                }
            },
            "phone": "010-692-6593 x09125",
            "website": "anastasia.net",
            "company": {
                "name": "Deckow-Crist",
                "catchPhrase": "Proactive didactic contingency",
                "bs": "synergize scalable supply-chains"
            }
        },
        {
            "id": 3,
            "name": "Clementine Bauch",
            "username": "Samantha",
            "email": "Nathan@yesenia.net",
            "address": {
                "street": "Douglas Extension",
                "suite": "Suite 847",
                "city": "McKenziehaven",
                "zipcode": "59590-4157",
                "geo": {
                    "lat": "-68.6102",
                    "lng": "-47.0653"
                }
            },
            "phone": "1-463-123-4447",
            "website": "ramiro.info",
            "company": {
                "name": "Romaguera-Jacobson",
                "catchPhrase": "Face to face bifurcated interface",
                "bs": "e-enable strategic applications"
            }
        },
        {
            "id": 5,
            "name": "Chelsey Dietrich",
            "username": "Kamren",
            "email": "Lucio_Hettinger@annie.ca",
            "address": {
                "street": "Skiles Walks",
                "suite": "Suite 351",
                "city": "Roscoeview",
                "zipcode": "33263",
                "geo": {
                    "lat": "-31.8129",
                    "lng": "62.5342"
                }
            },
            "phone": "(254)954-1289",
            "website": "demarco.info",
            "company": {
                "name": "Keebler LLC",
                "catchPhrase": "User-centric fault-tolerant solution",
                "bs": "revolutionize end-to-end systems"
            }
        },
        {
            "id": 7,
            "name": "Kurtis Weissnat",
            "username": "Elwyn.Skiles",
            "email": "Telly.Hoeger@billy.biz",
            "address": {
                "street": "Rex Trail",
                "suite": "Suite 280",
                "city": "Howemouth",
                "zipcode": "58804-1099",
                "geo": {
                    "lat": "24.8918",
                    "lng": "21.8984"
                }
            },
            "phone": "210.067.6132",
            "website": "elvis.io",
            "company": {
                "name": "Johns Group",
                "catchPhrase": "Configurable multimedia task-force",
                "bs": "generate enterprise e-tailers"
            }
        },
        {
            "id": 8,
            "name": "Nicholas Runolfsdottir V",
            "username": "Maxime_Nienow",
            "email": "Sherwood@rosamond.me",
            "address": {
                "street": "Ellsworth Summit",
                "suite": "Suite 729",
                "city": "Aliyaview",
                "zipcode": "45169",
                "geo": {
                    "lat": "-14.3990",
                    "lng": "-120.7677"
                }
            },
            "phone": "586.493.6943 x140",
            "website": "jacynthe.com",
            "company": {
                "name": "Abernathy Group",
                "catchPhrase": "Implemented secondary concept",
                "bs": "e-enable extensible e-tailers"
            }
        },
        {
            "id": 9,
            "name": "Glenna Reichert",
            "username": "Delphine",
            "email": "Chaim_McDermott@dana.io",
            "address": {
                "street": "Dayna Park",
                "suite": "Suite 449",
                "city": "Bartholomebury",
                "zipcode": "76495-3109",
                "geo": {
                    "lat": "24.6463",
                    "lng": "-168.8889"
                }
            },
            "phone": "(775)976-6794 x41206",
            "website": "conrad.com",
            "company": {
                "name": "Yost and Sons",
                "catchPhrase": "Switchable contextually-based project",
                "bs": "aggregate real-time technologies"
            }
        },
        {
            "id": 10,
            "name": "Clementina DuBuque",
            "username": "Moriah.Stanton",
            "email": "Rey.Padberg@karina.biz",
            "address": {
                "street": "Kattie Turnpike",
                "suite": "Suite 198",
                "city": "Lebsackbury",
                "zipcode": "31428-2261",
                "geo": {
                    "lat": "-38.2386",
                    "lng": "57.2232"
                }
            },
            "phone": "024-648-3804",
            "website": "ambrose.net",
            "company": {
                "name": "Hoeger LLC",
                "catchPhrase": "Centralized empowering task-force",
                "bs": "target end-to-end models"
            }
        }
    ];
    expect(await services.search("suite")).toEqual(expectedResult);
});

//Tests the function resposible for save a document into mutante index.
test("ElasticSearch save function test", async () => {
    expect(await ElasticSearch.add("/test", {}, "127.0.0.1", 200)).toEqual(201);
});