let express = require("express");
let consign = require("consign");
let app = express();
let port = process.env.YOUR_PORT || process.env.PORT || 8080;
let host = process.env.YOUR_HOST || "0.0.0.0";

consign()
    .include("app/routes")
    .include("app/controllers")
    .include("app/services")
    .include("config/elasticSearch")
    .into(app);
    
module.exports = {
    app: app,
    port: port,
    host: host
}