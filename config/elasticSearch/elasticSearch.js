let { Client } = require("@elastic/elasticsearch")
let client = new Client({ node: ((process.env.ES_HOST || "http://localhost") + ":" + (process.env.ES_PORT || "9200")) })

let database = {}

//Tries to create a new indice if there isn't one already.
client.indices.create({
    index: "mutant"
}, (error, resp) => {
    switch (resp.statusCode) {
        case 200:
            console.log("Created a new indice on elasticSearch.");
            break;
        case 400:
            console.log("The indice is already defined on elasticSearch.");
            break;
    }
});

//Saves a new document
database.add = async (endpoint, queryParams, request, response_status) => {
    let today = new Date();
    let dateTime = today.getDate()+"/"+(today.getMonth()+1)+"/"+today.getFullYear()+' '+today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    return await client.index({
        index: "mutant",
        id: + new Date(),
        type: "requests",
        body: {
            "endpoint": endpoint,
            "query_params": queryParams,
            "request_ip": request,
            "response_status": response_status,
            "when": dateTime
        }
    }).then((resp) => {
        return resp.statusCode;
    }).catch((error) => {
        console.error("Error while saving API log. Error =", error);
    });;
}


module.exports = database